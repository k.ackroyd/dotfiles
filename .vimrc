set visualbell
set visualbell t_vb=""
set background=dark " background colour
set t_Co=256
syntax on
" set nu
" set rnu
set cursorline

" colors gummybears
" colors torte " set the colour scheme
" colors desert
" colors nova
" colors smyck
" colors noctu
" colors inkpot
" colors kevnova
packadd! dracula
colors dracula
" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set t_kb=

" set autoindent " make block indents
set ts=2 " set the tab stops
set sw=2 " set the shift width
set softtabstop=2
set et
" set noet
set backspace=indent,eol,start " backspace goes wraps lines
set showcmd " Show (partial) command in status line.
set showmatch " Show matching brackets.
set smartcase " Do smart case matching (Case sensitive if an upper case char is included)
set incsearch " Incremental search
set autowrite " Automatically save before commands like :next and :make
set hidden " Hide buffers when they are abandoned
" set mouse=a " Enable mouse usage (all modes) in terminals
set ru " set the ruler
" set textwidth=80 " set the wrapping width
set antialias " font antialias
set encoding=utf-8 " file format encoding
" setlocal spell spelllang=en_gb
set linebreak
set history=1000
set viminfo='20,<1000
set laststatus=2
" enable list mode to show us trailing whitespace and tabs
set list
" set the characters that will represent tabs and trailing spaces
set listchars=tab:>-,trail:-
set rtp+=/usr/local/opt/fzf
ab kack kevin.ackroyd@sky.uk

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif
:source $VIMRUNTIME/macros/matchit.vim
filetype plugin on
execute pathogen#infect()
let g:vim_markdown_folding_disabled=1
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
let g:airline_theme='cool'
let g:pydiction_location = '$HOME/.vim/bundle/pydiction/complete-dict'
setlocal cm=blowfish2
" Syntastic Configuration
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
"
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" Nerdtree
map <C-n> :NERDTreeToggle<CR>
set hlsearch
set undofile
set undodir=~/.vim/undodir

" let g:ale_lint_on_text_changed = 'never'
" let g:ale_lint_on_enter = 0
let vim_markdown_preview_perl=1
let vim_markdown_preview_browser='Google Chrome'

nnoremap <F5> :Buffers<CR>
nnoremap <F6> :Files<CR>
nnoremap , :nohlsearch<CR>
nnoremap html :-1read $HOME/.vim/.skel.html<CR>6jwf>a
nnoremap py :-1read $HOME/.vim/.skel.py<CR>2j3la
nnoremap spell :w!:!aspell check %:e! %
nnoremap Q gwap

