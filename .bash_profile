if [ -f ~/.bashrc ]; then
    . .bashrc
fi
#export FZF_DEFAULT_COMMAND='ag -g ""'
#export FZF_CTRL_T_COMMAND='ag -g ""'
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
bind -x '"\C-p": vim $(fzf);'

#function _update_ps1() {
    #PS1=$(powerline-shell $?)
#}
#powerline-daemon -q

#if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    #PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
#fi
eval "$(starship init bash)"

if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi
source /usr/local/opt/helm@2/etc/bash_completion.d/helm
source ~/.kube/kubectl_autocompletion
source ~/.fzf.bash

export PATH="/usr/local/opt/helm@2/bin:$PATH"
complete -F __start_kubectl k
